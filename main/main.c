#include <stdio.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_intr_alloc.h"

#include "driver/gpio.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "led/led.h"
#include "button/button.h"
#include "flash/FlashMemory.h"
#include "wifi/WifiConfig.h"

#define LONG_PRESS_TIME 3000 // Button long press time in milliseconds


bool keyPressDetected 	= false,
     keyPressDown 		= false;

TickType_t keyPressStartTime = 0;
uint32_t counterLedblink =0;


BiColorLED led;
extern const int BTN_AP;

void button(void *pvParameter) {
    while (1) {
        if (keyPressDetected) {

            TickType_t keyPressDuration = xTaskGetTickCount() - keyPressStartTime;
            if (keyPressDuration >= pdMS_TO_TICKS(LONG_PRESS_TIME) && gpio_get_level(BTN_AP) ==0) {
                keyPressDown = true;

            }
        }

        if (keyPressDown) {

            printf("LONG PRESS\r\n");
            keyPressDown = false;
            keyPressDetected = false;
            keyPressStartTime =0;
            wifi_sta_config("MeshCasa", "felipeepamela1101");

        }

        vTaskDelay(100 / portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}





void app_main() {

	initializeButton();

    initializeTimer0();

    initializeLed();

    flashMemoryInitializer();

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    nvs_flash_init();

    initialize_wifi();

	ledConstructor(&led);
	led.turnOnRed();

    xTaskCreatePinnedToCore(button, "button", 8192, NULL, 10, NULL,1);
     int status =0;
    while(1){

        if(counterLedblink>5000){
            counterLedblink=0;
            status = !status;
            led.toggleLed(RED);

        }
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }

}


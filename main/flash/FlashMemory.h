/*
 * FlashMemory.h
 *
 *  Created on: 23 de nov. de 2023
 *      Author: Felipe
 */

#ifndef MAIN_FLASHMEMORY_H_
#define MAIN_FLASHMEMORY_H_


void flashMemoryInitializer();
void verifyFile(char* path);
char* readFile(char* path);
void writeFile(char* state,char* path);

#endif /* MAIN_FLASHMEMORY_H_ */

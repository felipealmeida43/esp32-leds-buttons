/*
 * FlashMemory.c
 *
 *  Created on: 23 de nov. de 2023
 *      Author: Felipe
 */
#include "FlashMemory.h"
#include "esp_spiffs.h"
#include "esp_log.h"
#include <stdio.h>
#include "stdint.h"
#include "string.h"
#include "nvs_flash.h"
#include "esp_spiffs.h"
#include "driver/uart.h"
//#include "Variables.h"
extern char line[64];
bool flagFileEmpty = false;
static const char *TAG2 = "MEMORY FLASH";
void verifyFile(char* path)
{
	char formatFile[50];
	sprintf(formatFile,"/spiffs/%s",path);
	FILE* file = fopen(formatFile, "r");
	if (file == NULL) {

		ESP_LOGI(TAG2, "Failed to verify file ");
		uart_write_bytes(UART_NUM_2, "Failed to verify file\r\n", strlen("Failed to verify file\r\n"));
		flagFileEmpty = true;
		//FILE* file = fopen(formatFile,"w");
		//fprintf(file, "%s\n","/?SSID=EDC_TESTE&pass=12345678&ID=0");
		//fclose(file);
		//return 0;
	}
	fclose(file);
}
char* readFile(char* path)
{
	char formatFile[50];
	sprintf(formatFile,"/spiffs/%s",path);
	FILE* file = fopen(formatFile, "r");
	if (file == NULL) {

		ESP_LOGE(TAG2, " READ FILE Failed to open file for reading");
		uart_write_bytes(UART_NUM_2, "READ FILE Failed to open file for reading\r\n", strlen("READ FILE Failed to open file for reading\r\n"));
		//sprintf(formatFile,"/spiffs/%s",path);
		flagFileEmpty = true;
		FILE* file = fopen(formatFile,"w");
		fprintf(file, "%s\n","&ssid=ROU004_TESTE&pass=12345678&ID=1");
		fclose(file);
		return 0;
	}
	//char line[64];
	fgets(line, sizeof(line), file);
	fclose(file);
	// strip newline
	char* pos = strchr(line, '\n');
	if (pos) {
		*pos = '\0';
	}
	/*
	 * Debu purposes to see which pass and ssid was previously saved
	char file_saved[100];
	sprintf(file_saved,"READ from file: [%s] \r\n",line);
	uart_write_bytes(UART_NUM_2, file_saved, strlen(file_saved));
	ESP_LOGI(TAG2, "Read from file: '%s'", line);
	*/
	return line;

}
void writeFile(char* state,char* path)
{
	char formatFile[50];
	sprintf(formatFile,"/spiffs/%s",path);
	FILE* file = fopen(formatFile,"w");
	if (file == NULL) {
		uart_write_bytes(UART_NUM_2, "Failed File written", strlen("Failed File written"));
		return;
	}
	fprintf(file, "%s\n",state);
	fclose(file);
	ESP_LOGI(TAG2, "File written");
	uart_write_bytes(UART_NUM_2, "File written", strlen("File written"));

}
void flashMemoryInitializer()
{
	esp_vfs_spiffs_conf_t conf = {
			.base_path = "/spiffs",
			.partition_label = NULL,
			.max_files = 5,
			.format_if_mount_failed = false

	};

	// Use settings defined above to initialize and mount SPIFFS filesystem.
	// Note: esp_vfs_spiffs_register is an all-in-one convenience function.
	esp_err_t ret = esp_vfs_spiffs_register(&conf);

	if (ret != ESP_OK)
	{
		if (ret == ESP_FAIL) {
			ESP_LOGE(TAG2, "Failed to mount or format filesystem");
		} else if (ret == ESP_ERR_NOT_FOUND) {
			ESP_LOGE(TAG2, "Failed to find SPIFFS partition");
		} else {
			ESP_LOGE(TAG2, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
		}
		size_t total = 0, used = 0;
	        ret = esp_spiffs_info(conf.partition_label, &total, &used);
	        if (ret != ESP_OK) {
	             ESP_LOGE(TAG2, "Failed to get SPIFFS partition information (%s). Formatting...", esp_err_to_name(ret));
	              esp_spiffs_format(conf.partition_label);
	              return;
	            } else {
	                ESP_LOGI(TAG2, "Partition size: total: %d, used: %d", total, used);
	            }
		return;
	}else ESP_LOGI(TAG2, "Mounted File System");
	ESP_LOGI(TAG2, "FLASH MEMORY INITIALIZED");
}

/*
 * button.h
 *
 *  Created on: 22 de fev. de 2024
 *      Author: Felipe
 */
#ifndef MAIN_BUTTON_BUTTON_H_
#define MAIN_BUTTON_BUTTON_H_

void ISR_Read_Inputs(void* channel);
void initializeButton();
void initializeTimer0();

#endif

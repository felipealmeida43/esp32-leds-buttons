/*
 * button.c
 *
 *  Created on: 22 de fev. de 2024
 *      Author: Felipe
 */

#include <stdio.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_intr_alloc.h"
#include "driver/gpio.h"

const int BTN_AP   =   4;
enum INPUT_CHANNELS{
  ID_0,
  NUMBER_OF_INPUT_CHANNELS
};
struct DigitalChannels {

  unsigned int pin;
  volatile long  count;
  volatile long  debounce;
  volatile long  debounce_target;
  volatile bool last_state;
  volatile bool current_state;
  volatile bool isPressed;
  void* id;

};
struct DigitalChannels Channel_0 = { BTN_AP, 0, 0, 50, 1, 1, (void*)ID_0 ,0};
/*************EXTERN VARIABLES****************************************/
extern TickType_t keyPressStartTime;
extern uint32_t counterLedblink;
extern bool keyPressDetected;
/**********************************************************************/
portMUX_TYPE ISR_MUX = portMUX_INITIALIZER_UNLOCKED;
void IRAM_ATTR ISR_Read_Inputs(void* channel) {
    if (channel == Channel_0.id) {
        portENTER_CRITICAL(&ISR_MUX);
        Channel_0.last_state = 0;
         Channel_0.debounce =1;

         keyPressStartTime = xTaskGetTickCount();
        gpio_isr_handler_remove(BTN_AP);
        portEXIT_CRITICAL(&ISR_MUX);
    }
}
static void ISR_HW_Timer_0(void *arg) {

	portENTER_CRITICAL(&ISR_MUX);

	  if (Channel_0.debounce > 0)
	  	  {
	  		  Channel_0.debounce++;

	  	     if (Channel_0.debounce > Channel_0.debounce_target)
	  	     {

	  	    	 Channel_0.current_state = gpio_get_level(BTN_AP);

	  	    	 if (Channel_0.last_state == Channel_0.current_state)
	  	    	 {

	  	    		 	Channel_0.isPressed = true;
                        keyPressDetected = true;
	  	                Channel_0.last_state = Channel_0.current_state;
	  	                Channel_0.count++;
	  	                Channel_0.debounce = 0;

	  	                gpio_isr_handler_add(BTN_AP, ISR_Read_Inputs, (void *)Channel_0.id);

	  	         }

	  	      }

	  	   }
      counterLedblink++;
	  portEXIT_CRITICAL(&ISR_MUX);
}
void initializeTimer0(){

	 const esp_timer_create_args_t timer1_args = {
			  		      .callback = &ISR_HW_Timer_0,
			  		      .name = "Timer 1"};
			  		esp_timer_handle_t timer1_handler;
			  		ESP_ERROR_CHECK(esp_timer_create(&timer1_args, &timer1_handler));
			  		ESP_ERROR_CHECK(esp_timer_start_periodic(timer1_handler, 80));

}
void initializeButton() {
    // Replace BTN_AP_PIN with the actual GPIO pin for your button
    gpio_config_t buttonConfig = {
        .pin_bit_mask = (1ULL<<BTN_AP),
        .mode = GPIO_MODE_INPUT,
        .intr_type = GPIO_INTR_NEGEDGE,
    };
    gpio_config(&buttonConfig);

    gpio_install_isr_service(ESP_INTR_FLAG_IRAM);
    gpio_isr_handler_add(BTN_AP, ISR_Read_Inputs, (void *)Channel_0.id);



}

/*
 * WifiConfig.h
 *
 *  Created on: 22 de fev. de 2024
 *      Author: Felipe
 */

#ifndef MAIN_WIFI_WIFICONFIG_H_
#define MAIN_WIFI_WIFICONFIG_H_

void initialize_wifi();
void wifi_sta_config(char* ssid_, char* pass_);


#endif /* MAIN_WIFI_WIFICONFIG_H_ */

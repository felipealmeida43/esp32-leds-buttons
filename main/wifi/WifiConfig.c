/*
 * WifiConfig.c
 *
 *  Created on: 22 de fev. de 2024
 *      Author: Felipe
 */
#include <string.h>

#include "driver/gpio.h"
#include "driver/uart.h"
#include "esp_mac.h"
#include "esp_log.h"
#include "stddef.h"
#include "esp_wifi.h"
#include "WifiConfig.h"
//#include "Button.h"
esp_netif_t *sta_netif = NULL;
esp_netif_t *ap_netif = NULL;

const char* TAG 			= "WIFI_CONFIG";
const char* HOSTNAME	 	= "WIFI-RINNAI";
char ssid[100];
char pass[100];

bool 	sta_mode,ap_mode = false,
		wifi_connected = false;

uint32_t countResetConnection =0;
#define MAX_RESET_CONN 10
void initialize_wifi()
{


	esp_netif_init();
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	ESP_ERROR_CHECK(esp_event_loop_create_default());
	sta_netif = esp_netif_create_default_wifi_sta();
	assert(sta_netif);


	ESP_LOGI(TAG, "Wifi Init Config");

}
static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{

	switch(event_id)
	{


		case WIFI_EVENT_STA_START:
			uart_write_bytes(UART_NUM_2, "WIFI STARTED\r\n", strlen("WIFI STARTED\r\n"));
			//ESP_LOGI(TAG, "WIFI STARTED\r\n");
			break;
		case WIFI_EVENT_STA_STOP:
			uart_write_bytes(UART_NUM_2, "WIFI STOPPED\r\n", strlen("WIFI STOPPED\r\n"));
			//ESP_LOGI(TAG, "WIFI STOPPED\r\n");
			break;
		case WIFI_EVENT_STA_CONNECTED:

					ap_mode = false;
					sta_mode = true;

					wifi_connected = true;

					uart_write_bytes(UART_NUM_2, "WIFI CONNECTED\r\n", strlen("WIFI CONNECTED\r\n"));

					break;
		case WIFI_EVENT_STA_DISCONNECTED:
			wifi_connected =false;

		uart_write_bytes(UART_NUM_2, "WIFI DISCONNECTED\r\n", strlen("WIFI DISCONNECTED\r\n"));

		if(!ap_mode /*&& !NetworkInfo.isBluetooth*/){
			esp_wifi_disconnect();
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			sta_mode = true;
			esp_err_t ret = esp_wifi_connect();
			uart_write_bytes(UART_NUM_2, "ERR: \r\n", strlen("ERR: \r\n"));
			char error[5];
			sprintf(error,"%d",ret);
			//itoa(ret,error,1);
			uart_write_bytes(UART_NUM_2,error,strlen(error));
			countResetConnection++;
			if(countResetConnection >MAX_RESET_CONN /*&& !NetworkInfo.isBluetooth*/){
				countResetConnection =0;
				uart_write_bytes(UART_NUM_2, "RESTART_ESP\r\n", strlen("RESTART_ESP\r\n"));
				esp_restart();
			}

		}
		//wifi_sta_config(ssid,pass);

		break;

		case IP_EVENT_STA_GOT_IP:
			ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP");
			uart_write_bytes(UART_NUM_2, "SYSTEM_EVENT_STA_GOT_IP", strlen("SYSTEM_EVENT_STA_GOT_IP"));
			/*
				if(!mqttAlreadyStarted){

					mqtt_app_start();
				}else {
					esp_mqtt_client_reconnect(client);
				}
				*/
			//}


			//xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);


			break;
		case IP_EVENT_STA_LOST_IP: wifi_sta_config(ssid,pass);
		uart_write_bytes(UART_NUM_2, "IP_EVENT_STA_LOST_IP", strlen("IP_EVENT_STA_LOST_IP"));
		break;
		case WIFI_EVENT_AP_START:
			ESP_LOGI(TAG, "AP START %u",event_id);
			//millis_led = 500;
			break;
		default: ESP_LOGI(TAG, "EVENT UNRECO %u",event_id);
			break;
	}
	//return ESP_OK;
}
void wifi_sta_config(char* ssid_, char* pass_)
{


	uart_write_bytes(UART_NUM_2, "WIFI CONFIG", strlen("WIFI CONFIG"));
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	wifi_sta_config_t sta_config ={};
	strcpy((char *)sta_config.ssid,(char*) ssid_);
	strcpy((char *)sta_config.password,(char*) pass_);
	sta_config.bssid_set = false;


	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA,(wifi_config_t*) &sta_config));
	esp_err_t ret = esp_netif_set_hostname(sta_netif,HOSTNAME);
	if(ret != ESP_OK ){
			ESP_LOGI(TAG,"failed to set hostname:%d",ret);
		}

	ESP_ERROR_CHECK(esp_wifi_start());

	esp_event_handler_instance_t instance_any_id;
	esp_event_handler_instance_t instance_got_ip;
	ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
	                                                        ESP_EVENT_ANY_ID,
	                                                        &event_handler,
	                                                        NULL,
	                                                        &instance_any_id));
	ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
	                                                        IP_EVENT_STA_GOT_IP,
	                                                        &event_handler,
	                                                        NULL,
	                                                        &instance_got_ip));



	vTaskDelay(1000 / portTICK_PERIOD_MS);
	//millis_led = 100;
	esp_wifi_connect();

	//ESP_ERROR_CHECK(esp_wifi_connect());
	uart_write_bytes(UART_NUM_2, "Finish Initial Config\r\n", strlen("Finish Initial Config\r\n"));

}


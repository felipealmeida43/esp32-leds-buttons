/*
 * led.h
 *
 *  Created on: 9 de jan. de 2024
 *      Author: Felipe
 */

#ifndef MAIN_LED_LED_H_
#define MAIN_LED_LED_H_

#define BT_CONNECTED	100
#define NETWORK_DISCON	10
#define WIFI_CONNECTED	500
enum{
	RED =0,
	GREEN,
	BOTH
};


typedef struct {
    void (*turnOnRed)(void);
    void (*turnOnGreen)(void);
    void (*turnOff)(void);
    void (*toggleLed)(int led);
} BiColorLED;

void turnOnRed(void);
void turnOnGreen(void);
void turnOff(void);
void toggleLed(int led);
int ledConstructor(BiColorLED * LedObject);
void initializeLed();
#endif /* MAIN_LED_LED_H_ */

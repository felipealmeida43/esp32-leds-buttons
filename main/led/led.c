/*
 * led.c
 *
 *  Created on: 9 de jan. de 2024
 *      Author: Felipe
 */
#include "driver/gpio.h"

#include "led.h"
#define LED_VM 18
#define LED_VD 19
const int LED_STATUS = 2;
int status = 0;
//extern int LED_STATUS;
void toggleLed(int led){

	status =!status;
	if(led == 1){
		gpio_set_level(LED_VD, status);
		gpio_set_level(LED_VM, 0);
	}else if(led == 0){

		gpio_set_level(LED_STATUS, status);
		gpio_set_level(LED_VD, 0);
	}
	else if(led == 2){

			gpio_set_level(LED_VM, status);
			gpio_set_level(LED_VD, !status);
		}
}
void turnOnRed(void) {
    gpio_set_level(LED_VM, 1);
    gpio_set_level(LED_VD, 0);
}

void turnOnGreen(void) {
    gpio_set_level(LED_VM, 0);
    gpio_set_level(LED_VD, 1);
}

void turnOff(void) {
    gpio_set_level(LED_VM, 0);
    gpio_set_level(LED_VD, 0);
}

BiColorLED createBiColorLED() {
    BiColorLED led;
    led.turnOnRed = 	turnOnRed;
    led.turnOnGreen = 	turnOnGreen;
    led.turnOff = 		turnOff;
    led.toggleLed = 	toggleLed;
    return led;
}
int ledConstructor(BiColorLED * LedObject) {
	       if(LedObject == NULL){
	             return 0;
	       }else{

	    	   LedObject->turnOff 		=	&turnOff;
	    	   LedObject->turnOnGreen 	= 	&turnOnGreen;
	    	   LedObject->turnOnRed 	=	&turnOnRed;
	    	   LedObject->toggleLed		=   &toggleLed;
	    	   return 1;
	       }
	}
void initializeLed(){
	gpio_set_direction(LED_STATUS, GPIO_MODE_OUTPUT);
		gpio_config_t io_config;                     // Descriptor Variable of GPIO driver
		io_config.intr_type = GPIO_INTR_DISABLE; // Disable interrupt resource
		io_config.mode = GPIO_MODE_OUTPUT;
		io_config.pin_bit_mask =(1ULL<<LED_STATUS) ;
		gpio_config(&io_config);
}
